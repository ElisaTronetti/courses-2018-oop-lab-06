/**
 * 
 */
package it.unibo.oop.lab.collections2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 
 * Instructions
 * 
 * This will be an implementation of
 * {@link it.unibo.oop.lab.collections2.SocialNetworkUser}:
 * 
 * 1) complete the definition of the methods by following the suggestions
 * included in the comments below.
 * 
 * @param <U>
 *            Specific user type
 */
public class SocialNetworkUserImpl<U extends User> extends UserImpl implements SocialNetworkUser<U> {

    /*
     * 
     * [FIELDS]
     * 
     * Define any necessary field
     * 
     * In order to save the people followed by a user organized in groups, adopt
     * a generic-type Map:
     * 
     * think of what type of keys and values would best suit the requirements
     */
	//creo una mappa che contiene come chiave il nome del gruppo e i valori sono gli amici che si trovano in quella classe
	Map<String, Set<U>> friends;
	
	
    /*
     * [CONSTRUCTORS]
     * 
     * 1) Complete the definition of the constructor below, for building a user
     * participating in a social network, with 4 parameters, initializing:
     * 
     * - firstName - lastName - username - age and every other necessary field
     * 
     * 2) Define a further constructor where age is defaulted to -1
     */

    /**
     * Builds a new {@link SocialNetworkUserImpl}.
     * 
     * @param name
     *            the user firstname
     * @param surname
     *            the user lastname
     * @param userAge
     *            user's age
     * @param user
     *            alias of the user, i.e. the way a user is identified on an
     *            application
     */
	
	 public SocialNetworkUserImpl(final String name, final String surname, final String user) {
	        super(name, surname, user, -1);
	 }
	 
    public SocialNetworkUserImpl(final String name, final String surname, final String user, final int userAge) {
        super(name, surname, user, userAge);
        this.friends = new HashMap<>();
    }

    /*
     * [METHODS]
     * 
     * Implements the methods below
     */

    @Override
    //metto in set gli amici che trovo sotto il nome circle della HashMap
    //se non esiste (== null) creo una nuova HashMap, con nome del gruppo e il gruppo di amici
    //infine aggiungo l'amico nella mappa
    public boolean addFollowedUser(final String circle, final U user) {
    	Set<U> circleFriend = this.friends.get(circle);
    	if ( circleFriend == null) {
    		this.friends = new HashMap<>();
    		this.friends.put(circle, circleFriend);
    		
    	}
        return circleFriend.add(user);
    }

    @Override
    //crea una nuova collezione che contiene tutti gli amici di un certo gruppo
    //se la collezione non risulta vuota allora torno un array list di tutti gli amici del gruppo
    //altrimenti torno una collezione vuota
    public Collection<U> getFollowedUsersInGroup(final String groupName) {
    	final Collection<U> groupFriend = this.friends.get(groupName);
    	if( groupFriend != null) {
    		return new ArrayList<>(groupFriend);
    	}
        return Collections.emptyList();
    }

    @Override
    //creo un nuovo HashSet e per ogni gruppo vado a cercare gli amici
    //quando li trovo li aggiungo nell'HashSet che avevo creato
    //ritorno un array list che contiene HashSet di tutti gli amici che avevo trovato
    public List<U> getFollowedUsers() {
    	final Set<U> followedUsers = new HashSet<>();
    	for (final Entry<String, Set<U>> group : friends.entrySet()) {
    		followedUsers.addAll(group.getValue());
    	}
    	return new ArrayList<>(followedUsers);
    }

}
