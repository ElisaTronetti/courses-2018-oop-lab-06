package it.unibo.oop.lab.collections1;

import java.util.*;


/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

	private static final int MAX_NUMBER = 2000;
	private static final int FIRST_POSITION = 0;
	private static final int LAST_POSITION = 999;
	private static final int ADD_FIRST_POSITION = 100000;
	private static final int NUMBER_READING = 1000;
	
	private static final long AFRICA_POPULATION = 1_110_635_000L;
	private static final long AMERICAS_POPULATION = 972_005_000L;
	private static final long ANTARTICA_POPULATION = 0L;
	private static final long ASIA_POPULATION = 4_298_723_000L;
	private static final long EUROPE_POPULATION = 742_452_000;
	private static final long OCEANIA_POPULATION = 38_304_000;
	
    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
    	
    	/*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	final List<Integer> arrayNumbers = new ArrayList<>();
    	
    	for (int i = 1000; i < MAX_NUMBER; i++) { //faccio un ciclo for che mi scorre tutti i numeri dal 1000 in poi e li vado ad aggiungere
    		arrayNumbers.add(i);
    	}
    	System.out.println("Array List elements: " + arrayNumbers);
    	
    	  /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	final List<Integer> linkedNumbers = new LinkedList<>();
    	linkedNumbers.addAll(arrayNumbers); //aggiunto nella LinkedList tutti gli elementi già presenti nell'ArrayList
    	
    	System.out.println("Linked List elements: " + linkedNumbers);
    	
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	//lavoro i vari indici per invertire i numeri della prima e dell'ultima posizione 
    	int first = arrayNumbers.get(FIRST_POSITION);
    	arrayNumbers.set(FIRST_POSITION, arrayNumbers.get(LAST_POSITION));
    	arrayNumbers.set(LAST_POSITION, first);   
    	
    	System.out.println("Swapped the firts and last element in the Array List: " + arrayNumbers);
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for(int n : arrayNumbers) {
    		System.out.println(n);
    	}
    	
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	long time = System.nanoTime(); //setto la variabile time
    	for(int i = FIRST_POSITION; i <= ADD_FIRST_POSITION; i++) { //aggiungo i 100 000 numeri nella prima posizione
    		arrayNumbers.add(FIRST_POSITION, i);
    	}
    	
    	time = System.nanoTime() - time; //vedo il tempo che ci ha messo e lo stampo nella sysout sotto
    	System.out.println("Adding " + ADD_FIRST_POSITION
                 + " numbers and inserting them in the first position in a ArrayList took: " + time
                 + " ns" );
    	
    	//stessa cosa con linkedlist
    	
    	long time1 = System.nanoTime(); //setto la variabile time
    	for(int i = FIRST_POSITION; i <= ADD_FIRST_POSITION; i++) { //aggiungo i 100 000 numeri nella prima posizione
    		linkedNumbers.add(FIRST_POSITION, i);
    	}
    	
    	time1 = System.nanoTime() - time1; //vedo il tempo che ci ha messo e lo stampo nella sysout sotto
    	System.out.println("Adding " + ADD_FIRST_POSITION
                 + " numbers and inserting them in the first position in a LinkedList took: " + time1
                 + " ns" );
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	long time2 = System.nanoTime();
    	for(int i = FIRST_POSITION; i <= NUMBER_READING; i++) {
    		arrayNumbers.get(LAST_POSITION / 2);
    	}
    	
    	time2 =  System.nanoTime() - time2;
    	System.out.println("Reading " + (LAST_POSITION / 2)
                + " position for " + NUMBER_READING + " times in a ArrayList took: " + time2
                + " ns" );
    	
    	//stessa cosa con LinkedList
    	long time3 = System.nanoTime();
    	for(int i = FIRST_POSITION; i <= NUMBER_READING; i++) {
    		linkedNumbers.get(LAST_POSITION / 2);
    	}
    	
    	time3 =  System.nanoTime() - time3;
    	System.out.println("Reading " + (LAST_POSITION / 2)
                + " position for " + NUMBER_READING + " times in a LinkedList took: " + time3
                + " ns" );
    	
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
    	//creo una nuova map, con chiave stringa e valore long(population) e la riempio usando put
    	final Map <String, Long> population = new HashMap<>();
    	population.put("Africa", AFRICA_POPULATION);
    	population.put("Americas", AMERICAS_POPULATION);
    	population.put("Antartica", ANTARTICA_POPULATION);
    	population.put("Asia", ASIA_POPULATION);
    	population.put("Europe", EUROPE_POPULATION);
    	population.put("Oceania", OCEANIA_POPULATION);
    	
    	System.out.println(population);
    	
        /*
         * 8) Compute the population of the world
         */
    	//faccio un ciclo che mi permette di aggiungere ogni valore(population) 
    	//alla variabile totPopulation, per avere il totale della popolazione del mondo
    	long totPopulation = 0;
    	for(long i: population.values()) {
    		totPopulation += i;
    	}
    	System.out.println("Total population of the world: " + totPopulation);
    }
}
