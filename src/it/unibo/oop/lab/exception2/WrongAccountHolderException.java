package it.unibo.oop.lab.exception2;

public class WrongAccountHolderException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int expectedAccount;
	private final int actualAccount;
	
	public WrongAccountHolderException(final int expectedAccount, final int actualAccount) {
		super();
		this.expectedAccount = expectedAccount;
		this.actualAccount = actualAccount;
	}
	
	public String getMessage() {
        return "The account holder " + this.actualAccount + " is not the account expected " + this.expectedAccount;
    }

}
