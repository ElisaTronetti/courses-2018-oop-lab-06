package it.unibo.oop.lab.exception2;

import org.junit.Test;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
    	
    	
    	AccountHolder hold1 = new AccountHolder("Mario", "Rossi", 12);
    	AccountHolder hold2 = new AccountHolder("Marco", "Bianchi", 70);
    	
    	StrictBankAccount acc1 = new StrictBankAccount(hold1.getUserID(), 20000, 2);
    	StrictBankAccount acc2 = new StrictBankAccount(hold2.getUserID(), 1000, 1);
    	
    	//faccio diversi blocchi try-catch lanciando le eccezioni che ho creato, di modo che io possa controllare che vengano lanciate correttamente
    	//stampano il messaggio che ho implementato in getMessage dentro le eccezioni
    	try {
    		acc1.depositFromATM(hold2.getUserID(), 100);//account holder sbagliato! ECCEZIONE
    	}
    	catch(WrongAccountHolderException e) {
    		System.out.println(e.getMessage());
    	}
    	
    	try {
    		for (int i = 1; i < 10; i++) {
    			acc1.depositFromATM(hold1.getUserID(), 100); //prelevo troppe volte dall'ATM. ECCEZIONE
    		}
    	}
    	catch(TransactionsOverQuotaException e) {
    		System.out.println(e.getMessage());
    	}
    	
    	try {
    		acc2.withdraw(hold2.getUserID(), 1001); //prelevo più soldi di quelli che ho. ECCEZIONE
    	}
    	catch(NotEnoughFoundsException e) {
    		System.out.println(e.getMessage());
    	}
    	
    }
}
