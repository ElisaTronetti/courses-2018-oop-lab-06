package it.unibo.oop.lab.exception2;

public class NotEnoughFoundsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final double balance;
	private final double amount;
	
	public NotEnoughFoundsException(final double balance, final double amount) {
		super();
		this.balance = balance;
		this.amount = amount;
	}
	
	public String getMessage() {
        return "Not enough founds. Your balance is " + this.balance + "but the amount you want is " + this.amount;
    }
	
}
