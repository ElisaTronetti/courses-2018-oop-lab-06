package it.unibo.oop.lab.exception2;

public class TransactionsOverQuotaException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int maxATM;

	public TransactionsOverQuotaException(final int maxATM) {
		super();
		this.maxATM = maxATM;
	}
	
	public String getMessage() {
        return "You have reached the maximum allowed number of operations by ATM is: " + this.maxATM;
    }
	
}
