package it.unibo.oop.lab06.generics1;

//c'era un altro pezzo in cui si doveva cercare il percorso da un nodo ad un altro
//in pratica è un algoritmo assurdo da implementare
//sinceramente è difficile e non so fare.

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class GraphImpl<C> implements Graph<C> {

	private Set<C> nodes;
	private Map<C, TreeSet<C>> edge;
	private TreeSet<C> connected;
	
	//costruttore che inizializza le collections
	public GraphImpl() {
		this.nodes = new TreeSet<>();
		this.edge = new HashMap<>();
	}
	
	//aggiunge tutti i nodi in un TreeSet
	public void addNode(C node) {
		this.nodes.add(node);
	}
	
	//COSE
	//ho creato una map, che contiene una chiave generica e come valori un TreeSet
	//questo perchè una stessa chiave può avere più nodi e non posso aggiungere più chiavi uguali
	public void addEdge(C source, C target) {
		if(!edge.containsKey(source)) { //se la chiave non c'è già
			this.connected = new TreeSet<>(); //creo il rispettivo TreeSet 
			connected.add(target); //gli inserisco il valore che voglio
			edge.put(source, connected); //e compilo la mappa, mettendo la chiave e un treeSet
		}
		else edge.get(source).add(target); //se invece la chiave esiste già, vado al rispettivo treeSet della chiave e gli aggiungo li il valore
	}

	//ritorno il set di nodi
	public Set<C> nodeSet() {
		return nodes;
	}
	
	//ritorno il set di angoli connessi al nodo specificato
	public Set<C> linkedNodes(C node) {
		return edge.get(node);
	}


}
