package it.unibo.oop.lab06.generics1;

/**
 *
 */
public final class UseGraph {

	private UseGraph() {
	}

	/**
	 * @param args
	 *            ignored
	 */
	public static void main(final String... args) {

		final Graph<String> g = new GraphImpl<>();

		g.addNode("a");
		g.addNode("b");
		g.addNode("c");
		g.addNode("d");
		g.addNode("e");

		//Should print ["a","b","c","d","e"], in any order
		System.out.println(g.nodeSet());

		g.addEdge("a", "b");
		g.addEdge("b", "c");
		g.addEdge("c", "d");
		g.addEdge("d", "e");
		g.addEdge("c", "a");
		g.addEdge("e", "a");
		//ne ho aggiunti due uguali per vedere se mi metteva in output due volte b
		//invece essendo un TreeSet, posso avere solo un elemento uguale
		//g.addEdge("d", "b");
		//g.addEdge("d", "b");
	
		//["d","a"], in any order
		System.out.println(g.linkedNodes("c"));

	}
}
